var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var boundary
var qtree
var p

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(boardSize, boardSize)

  boundary = new Rectangle(boardSize * 0.45, boardSize * 0.45, boardSize * 0.45, boardSize * 0.45)
  qtree = new QuadTree(boundary, 1, Math.floor(Math.random() * 256))
}

function draw() {
  createCanvas(boardSize, boardSize)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(boardSize * 0.5, boardSize * 0.5, boardSize, boardSize)

  if (mouseIsPressed) {
    var m = new Point(mouseX + random(-5, 5) - boardSize * 0.05, mouseY + random(-5, 5) - boardSize * 0.05)
    qtree.insert(m)
  }


  push()
  translate(boardSize * 0.5 - boardSize * 0.45, boardSize * 0.5 - boardSize * 0.45)
  qtree.show()
  pop()
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(boardSize, boardSize)
}

class Point {
  constructor(x, y) {
    this.x = x
    this.y = y
  }
}

class Rectangle {
  constructor(x, y, w, h) {
    this.x = x
    this.y = y
    this.w = w
    this.h = h
  }

  contains(point) {
    return (point.x >= this.x - this.w && point.x <= this.x + this.w && point.y >= this.y - this.h && point.y <= this.y + this.h)
  }
}

class QuadTree {
  constructor(boundary, n, c) {
    this.boundary = boundary
    this.capacity = n
    this.points = []
    this.divided = false
    this.c = c
    this.offset = Math.random() * Math.PI * 2
  }

  subdivide() {
    var ne = new Rectangle(this.boundary.x + this.boundary.w * 0.5, this.boundary.y - this.boundary.h * 0.5, this.boundary.w * 0.5, this.boundary.h * 0.5)
    this.northeast = new QuadTree(ne, this.capacity, Math.floor(Math.random() * 256))
    var nw = new Rectangle(this.boundary.x - this.boundary.w * 0.5, this.boundary.y - this.boundary.h * 0.5, this.boundary.w * 0.5, this.boundary.h * 0.5)
    this.northwest = new QuadTree(nw, this.capacity, Math.floor(Math.random() * 256))
    var se = new Rectangle(this.boundary.x + this.boundary.w * 0.5, this.boundary.y + this.boundary.h * 0.5, this.boundary.w * 0.5, this.boundary.h * 0.5)
    this.southeast = new QuadTree(se, this.capacity, Math.floor(Math.random() * 256))
    var sw = new Rectangle(this.boundary.x - this.boundary.w * 0.5, this.boundary.y + this.boundary.h * 0.5, this.boundary.w * 0.5, this.boundary.h * 0.5)
    this.southwest = new QuadTree(sw, this.capacity, Math.floor(Math.random() * 256))
    this.divided = true
  }

  insert(point) {
    if (!this.boundary.contains(point)) {
      return false
    }
    if (this.points.length < this.capacity) {
      this.points.push(point)
      return true
    } else {
      if (!this.divided) {
        this.subdivide()
      }
      if (this.northeast.insert(point)) {
        return true
      } else if (this.northwest.insert(point)) {
        return true
      } else if (this.southeast.insert(point)) {
        return true
      } else if (this.southwest.insert(point)) {
        return true
      }
    }
  }

  show() {
    fill(this.c * abs(sin(frameCount * 0.05 + this.offset)))
    stroke(this.c * abs(sin(frameCount * 0.05 + this.offset)))
    strokeWeight(boardSize * 0.001)
    ellipse(this.boundary.x, this.boundary.y, this.boundary.w * 2)
    if (this.divided) {
      this.northwest.show()
      this.northeast.show()
      this.southwest.show()
      this.southeast.show()
    }
  }
}
